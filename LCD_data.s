	;Sending data via DB7-DB3.
	
	;/*-------------------- LCD interface hardware definitions --------------------*/
	;/* PINS - See LCD_init.s for detail */
	
	;/* Register used as input parameter */
	; {R0, R1, R2, R3}
	;	R0 - Data content
	;	R1 - LCD Mode (RS=1)
	;	R2 - Not used
	;	R3 - Not used
	
	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE LPC23xx.inc	;MMR definitions
	EXPORT	LCD_data
	IMPORT	BusyWait
	IMPORT	LCD_4bit

DELAY	EQU		500
RS		EQU		1	;RS = 1 for Data mode

LCD_data
	STMIA	SP!, {R14}
	
	MOV		R1, #RS		;Set LCD Mode
	BL		LCD_4bit
	
	LDMDB	SP!, {PC}

	END