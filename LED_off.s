	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE	LPC23xx.inc ; MMR definitions
	EXPORT LED_off
		

LED_off
		STMIA	SP!, {R4, R5, LR}
		
		MOV		r5, #0xFF			; Set to Output
		LDR		R4, =(FIO0PIN0)
		STRB	R5, [R4] ;; LED turns on after this instruction
		
		MOV		R5, #0x00 ;All off...
		LDR		R4, =(FIO0DIR0)
		STRB	R5, [R4]

		LDMDB	SP!, {R4, R5, PC}
		END