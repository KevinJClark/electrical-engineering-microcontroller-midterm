;Exception handler for External Interrupt 1

	;/* Register used as input parameter */
	; {R0, R1, R2, R3}
	;	R0 - Not used
	;	R1 - Not used
	;	R2 - Key code from Keypad.s
	;	R3 - Signal EINT0_handler has been processed
	
	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE LPC23xx.inc ; MMR definitions
	EXPORT	EINT1_Handler
	IMPORT	BusyWait
	IMPORT	KeyPadHandler
	IMPORT	LED_Handler
	IMPORT	LCD_data
	IMPORT	LCD_command
	
EINT1_INTMASK	EQU		0x00004000
SIG_EN		RN	7	

EINT1_Handler
		SUBS	LR, LR, #4			;Proper Link register handling for IRQ
		STMIA	SP!, {R4-R9, LR}	;Preserve R4 thru R9 register
		
		LDR		r4, =(VICIntEnClr)	; Select VIC int enable reg
		MOV		r5, #EINT1_INTMASK		
		STR		r5, [r4]

entry
		MOV		R12, #0x20000
		BL		BusyWait
		
		MOV		r3, #1	;Signal of handler exiting
		BL		KeyPadHandler
		BL		LED_Handler
		
		
		; Clear VICVectAddr, see User manual 6.5.11 for detail
		LDR		r4, =(VICVectAddr)
		MOV		r5, #0
		STR		r5, [r4]
		
		LDR		r4, =(EXTINT)	;Clear External Interrupt flag
		LDR		r5, [r4]
		LDR		r6, =0x1				
		ORR		r5, r5, r6
		STR		r5, [r4]
		
exit
						
		LDMDB	SP!, {R4-R9, PC}^	;Restore R4 thru R9
	END