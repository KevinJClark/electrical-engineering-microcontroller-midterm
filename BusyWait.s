
	;General purpose delay loop using busy wait
	;Register used:
	;	R12 (Corruptable)
	
	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	EXPORT	BusyWait

BusyWait

	STMIA	SP!, {R14}
delay
	SUBS	R12, R12, #1
	BNE		delay
	
	LDMDB	SP!, {PC}
	END
	