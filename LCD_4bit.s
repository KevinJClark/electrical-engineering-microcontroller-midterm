	;Send a command via DB7-DB0.
	;Clear RS = 0 (command)
	;Set E = 1.  (Do not send RS = 0 and E =1 at the same time!  Must be 2 steps.)
	;Delay.
	;Clear E = 0.
	;Delay.
	;...
	;Set E = 1.  (Do not send RS = 0 and E =1 at the same time!  Must be 2 steps.)
	;Delay.
	;Clear E = 0.
	;

	
	;/*-------------------- LCD interface hardware definitions --------------------*/
	;/* PINS - see LCD_init.s for detail
	
	;Assumption - Assume all hardware pins have been properly initialized
	
	;/* Register used as input parameter */
	; {R0, R1, R2, R3}
	;	R0 - Content
	;	R1 - Mode
	;	R2 - Not used
	;	R3 - Not used

	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE LPC23xx.inc	;MMR definitions
	EXPORT	LCD_4bit
	IMPORT	BusyWait

MODE_CMD	EQU		0
MODE_DATA	EQU		1

OFFSET_E	EQU		31
OFFSET_RW	EQU		29
OFFSET_RS	EQU		28
OFFSET_DB	EQU		24

DELAY		EQU		500

PIN_EN		RN	7
payload		RN	6

LCD_4bit

	STMIA	SP!, {R4-R7, R14}	;Preserve R4 thru R7 register
	
	MOV		PIN_EN, #0x01
	
	;===== Begin Data loading sequence =====
	LDR		R4, =(FIO1CLR)
	LDR		R5, =0xB0000000	 ;Clear E, RS, and R/W (write) => 1011 0000
	STR		R5, [R4]		 ;	P1.31, P1.29, P1.28
	
	TEQ		R1, #MODE_DATA
	BNE		Load_UpperDB

Data_Mode
	;=== Set RS (Data mode) ===
	LDR		R4, =(FIO1SET)
	MOV		R5, PIN_EN, LSL #OFFSET_RS
	STR		R5, [R4]
	
Load_UpperDB

	;=== Split data into upper 4-bit
	MOV		payload, R0, LSR #4	
	AND		payload, #0x0F

	;=== Clear DB PINs ===
	LDR		R4, =(FIO1CLR)
	MOV		R5, #0x0F000000
	STR		R5, [R4]

	;=== Send lower 4-bit
	LDR		R4, =(FIO1SET)
	MOV		R5, payload, LSL #OFFSET_DB
	STR		R5, [R4]

	;=== Delay
	LDR		R12, =DELAY
	BL		BusyWait

	;=== Set E ===
	LDR		R4, =(FIO1SET)
	MOV		R5, PIN_EN, LSL #OFFSET_E
	STR		R5, [R4]

	;=== Delay
	LDR		R12, =DELAY
	BL		BusyWait
	
	;=== Clear E ===
	LDR		R4, =(FIO1CLR)
	MOV		R5, PIN_EN, LSL #OFFSET_E	;Clear only E pin
	STR		R5, [R4]


Load_LowerDB

	;=== Split data into lower 4-bit
	MOV		payload, R0			
	AND		payload, #0x0F
	
	;=== Clear DB PINs ===
	LDR		R4, =(FIO1CLR)
	MOV		R5, #0x0F000000
	STR		R5, [R4]

	;=== Send lower 4-bit
	LDR		R4, =(FIO1SET)
	MOV		R5, payload, LSL #OFFSET_DB
	STR		R5, [R4]

	;=== Delay
	LDR		R12, =DELAY
	BL		BusyWait

	;=== Set E ===
	LDR		R4, =(FIO1SET)
	MOV		R5, PIN_EN, LSL #OFFSET_E
	STR		R5, [R4]

	;=== Delay
	LDR		R12, =DELAY
	BL		BusyWait
	
	;=== Clear E ===
	LDR		R4, =(FIO1CLR)
	MOV		R5, PIN_EN, LSL #OFFSET_E	;Clear only E pin
	STR		R5, [R4]


Post_processing
	;=== delay to allow LCD to process information ===
	LDR		R12, =100000
	BL		BusyWait

	LDMDB	SP!, {R4-R7, PC}	;Restore R4 thru R7 register

	END