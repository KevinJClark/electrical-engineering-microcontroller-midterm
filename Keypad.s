;Turn On/Off LEDs based on EINT0's IRQ status

	;/* Register used as input parameter */
	; {R0, R1, R2, R3}
	;	R0 - Not used
	;	R1 - Not used
	;	R2 - Key code to be converted to ASCII
	;	R3 - Used in EINT0_handler
	
	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE LPC23xx.inc ; MMR definitions
	EXPORT KeyPadHandler
	IMPORT LCD_data
	IMPORT LCD_command
	IMPORT BusyWait

EINT0_INTMASK	EQU		0x00004000

ROW1	EQU		2_1000
ROW2	EQU		2_0100
ROW3	EQU		2_0010
ROW4	EQU		2_0001

COL1	EQU		2_0111
COL2	EQU		2_1011
COL3	EQU		2_1101
COL4	EQU		2_1110
DELAY 	EQU		255

KeyPadHandler
		STMIA	SP!, {R4-R9, LR}	;Preserve R4 thru R9 register
		
		;;========newline_and_cls_code====
		;ADD 	R10, R10, #1
		
		;CMP 	R10, #17
		;BNE 	skip_nl
		;MOV		R0, #0xC0 ;\n
		;BL		LCD_command
		
;skip_nl
		;CMP 	R10, #33
		;BNE		skip_cls
		;MOV		R0, #0x01 ;cls
		;BL		LCD_command
		;MOV		R10, #1		;Set counter to 0
;skip_cls
		;;========newline_and_cls_code====
		
		LDR		R4, =(FIO4PIN0)
		LDR		R7, =(FIO4SET0) ;;R7 is fioset mem loca
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4]
		AND		R5, R5, #0xF ;; Care only about the last 4 bits
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		CMP		R5, #COL1 ;;?
		BEQ		_col1
		CMP		R5, #COL2
		BEQ		_col2
		CMP		R5, #COL3
		BEQ		_col3
		CMP		R5, #COL4
		BEQ		_col4

		
		;; Fallthrough -- Something wrong
		;MOV 	R0, #'X'
		;BL 		LCD_data
		B		exit

		
_col1
		MOV	R0, #'H' ;Fall Through
		
		MOV		R6, #0x70 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL1
		MOVEQ	R0, #'D'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		
		
		MOV		R6, #0xB0 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL1
		MOVEQ	R0, #'#'
		

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		MOV		R6, #0xD0 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL1
		MOVEQ	R0, #'0'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		
		
		MOV		R6, #0xE0 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL1
		MOVEQ	R0, #'*'
;;;;;;;;START

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		B		exit
_col2
		MOV	R0, #'J' ;Fall Through

		MOV		R6, #0x70 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL2
		MOVEQ	R0, #'C'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		
		
		MOV		R6, #0xB0 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL2
		MOVEQ	R0, #'9'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		
		
		MOV		R6, #0xD0 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL2
		MOVEQ	R0, #'8'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		
		
		MOV		R6, #0xE0 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL2
		MOVEQ	R0, #'7'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO

		B		exit
_col3
		MOV	R0, #'K' ;Fall Through

		MOV		R6, #0x70 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL3
		MOVEQ	R0, #'B'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		
		
		MOV		R6, #0xB0 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL3
		MOVEQ	R0, #'6'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		
		
		MOV		R6, #0xD0 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL3
		MOVEQ	R0, #'5'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		
		
		MOV		R6, #0xE0 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL3
		MOVEQ	R0, #'4'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO

		B		exit
_col4
		MOV	R0, #'L' ;Fall Through

		MOV		R6, #0x70 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL4
		MOVEQ	R0, #'A'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		
		
		MOV		R6, #0xB0 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL4
		MOVEQ	R0, #'3'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		
		
		MOV		R6, #0xD0 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL4
		MOVEQ	R0, #'2'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		
		
		MOV		R6, #0xE0 ;Set output (with respect to microcontroller) bits to "0xX"
		STR		R6, [R7]  ;Push into MMIO
		
		;====DELAY======
		MOV		R12, #DELAY
		BL		BusyWait
		
		LDR		R5, [R4] ;Check results
		AND		R5, R5, #0x0F ; Last 4 bits -- Input pins
		CMP		R5, #COL4
		MOVEQ	R0, #'1'

		MOV		R6, #0x00 ;RESTORE output (with respect to microcontroller) bits to "0x0"
		STR		R6, [R4]  ;Push into MMIO
		
		

		B		exit
		
		
exit	
		BL 		LCD_data ; Print data to LCD
		
		MOV		R12, #1048576
		BL		BusyWait
		
		
		LDMDB	SP!, {R4-R9, PC}
	END