;Turn On/Off LEDs based on EINT0's IRQ status

	;/* Register used as input parameter */
	; {R0, R1, R2, R3}
	;	R0 - Not used
	;	R1 - Not used
	;	R2 - Not used
	;	R3 - Used in EINT0_handler
	
	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE LPC23xx.inc ; MMR definitions
	EXPORT	LED_Handler

EINT0_INTMASK	EQU		0x00004000

LED_Handler
		STMIA	SP!, {LR}
		
		LDR		r4, =(VICIRQStatus)
		LDR		r5, [r4]

		TST		r5, #EINT0_INTMASK
		BEQ		LED_OFF
LED_ON
		;Test LED output
		LDR		r4, =(FIO2SET0)		; Select P2.0 to P2.7
		MOV		r5, #0xAA			; Set data
		STR		r5, [r4]
		B		exit
LED_OFF
		;Test LED output
		LDR		r4, =(FIO2CLR0)		; Select P2.0 to P2.7
		MOV		r5, #0xAA			; Set data
		STR		r5, [r4]
exit
		LDMDB	SP!, {PC}
	END