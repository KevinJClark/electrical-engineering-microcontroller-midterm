# Electrical-Engineering-Microcontroller-Midterm

Pure ARM assembly to control an LED's duty cycle and frequency using a keypad and timer with interrupts.  

Assembled/compiled with [ARM Compiler 5.06 update 6 (build 750)](https://developer.arm.com/tools-and-software/embedded/arm-compiler/downloads/version-5).  Keil uvision was the IDE used for this project.  The board shown below is the [MCB2370 with NXP LPC2378 microcontroller](http://www.keil.com/support/man/docs/mcb2300/mcb2300_intro.htm).

![midterm](/uploads/7de445a77d4b2a8e57e3102a6b2e93e1/midterm.jpg)