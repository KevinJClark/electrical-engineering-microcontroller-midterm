	;Send a command via DB7-DB0.
	
	;/*-------------------- LCD interface hardware definitions --------------------*/
	;/* PINS - see LCD_init.s for detail
	
	;Assumption - Assume all hardware pins have been properly initialized
	
	;/* Register used as input parameter */
	; {R0, R1, R2, R3}
	;	R0 - Command content
	;	R1 - LCD Mode (RS = 0)
	;	R2 - Not used
	;	R3 - Not used

	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE LPC23xx.inc	;MMR definitions
	EXPORT	LCD_command
	IMPORT	BusyWait
	IMPORT	LCD_4bit

DELAY	EQU		500
RS		EQU		0	;RS = 0 for Command mode

LCD_command

	STMIA	SP!, {R14}	;Preserve R4 thru R7 register
	
	MOV		R1, #RS		;Set LCD Mode
	BL		LCD_4bit
	
	LDMDB	SP!, {PC}	;Restore R4 thru R7 register

	END
