	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE	LPC23xx.inc ; MMR definitions
	EXPORT SCR1_code
	IMPORT	LED_on
	IMPORT	LED_off
	;Takes input from R8, Pulse width

SCR1_code
		STMIA	SP!, {R0, R2, LR}
		
		; Set the value of the Match Register (page 558)
		LDR		R0, =(T2MR0) ;Match register 0
		STR		R8, [R0] ;Push value in R8 into MR0
		MOV		R8, #0x0 ;Zero out R8
		LDMDB	SP!, {R0, R2, PC}
		END