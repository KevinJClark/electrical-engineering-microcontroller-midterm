;Exception handler for External Interrupt 0

	;/* Register used as input parameter */
	; {R0, R1, R2, R3}
	;	R0 - Not used
	;	R1 - Not used
	;	R2 - Key code from Keypad.s
	;	R3 - Signal EINT0_handler has been processed
	
	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE LPC23xx.inc ; MMR definitions
	EXPORT	TIMER2_Handler
	IMPORT	LED_on
	IMPORT	LED_off
	IMPORT	BusyWait
	
TIMER2_INTMASK	EQU		0x04000000 ;26th bit = 1

TIMER2_Handler
		SUBS	LR, LR, #4			;Proper Link register handling for IRQ
		STMIA	SP!, {R4-R8, LR}	;Preserve R4 thru R8 register
		
		MOV		R12, #255
		BL		BusyWait
		
		LDR R4, =(T2IR)
		LDR	R5, [R4]
		
		MOV		R12, #255
		BL		BusyWait
		
		CMP		R9, #1 ;LED state flag
			MOVEQ R9, #0
			MOVNE R9, #1
		BLEQ	LED_off
		BLNE	LED_on
			;BLEQ	LED_off
		;BLNE	LED_on
			
		
			
			
		
		; Clear VICVectAddr, see User manual 6.5.11 for detail
		LDR		r4, =(VICVectAddr)
		MOV		r5, #0
		STR		r5, [r4]
		
		LDR		r4, =(VICIntEnClr)	; Select VIC int clear reg
		MOV		r5, #TIMER2_INTMASK		
		STR		r5, [r4] ;Disable TIMER2
		
		
	
		MOV		R12, #255
		BL		BusyWait
		
		LDR 	R4, =(T2IR)
		MOV		R5, #0xFF
		STR		R5, [R4]
		
		MOV		R12, #255
		BL		BusyWait
		
		MOV		R3,	#2
		
		LDR R4, =(T2IR)
		LDR	R5, [R4]
		
exit
						
		LDMDB	SP!, {R4-R8, PC}^	;Restore R4 thru R8
	END