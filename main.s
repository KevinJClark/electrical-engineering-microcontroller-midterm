; Main routine
; Use On-board switch (button) S2 located next to the Reset button
; According to the schematic, S2 is connected to P2.10

	AREA  FLASH,  CODE,  READONLY
	ENTRY
	ARM

	INCLUDE LPC23xx.inc ; MMR definitions
	EXPORT	__main
	IMPORT	LCD_init
	IMPORT	EINT0_Handler
	IMPORT	LCD_data
	IMPORT	LCD_command
	IMPORT	KeyPadHandler
	IMPORT	BusyWait
	IMPORT	LCD_string
	IMPORT	timer2_init
	IMPORT	TIMER2_Handler
	IMPORT	LED_on
	IMPORT	LED_off
	IMPORT	SCR1_code
	IMPORT	SCR2_code
	
EN				EQU		01
PIN_EINT0_OFS	EQU		20
EINT0_INTMASK	EQU		0x00004000 ;14th bit
TIMER2_INTMASK	EQU		0x04000000 ;26th bit = 1

SIG_EN		RN	7
STR_PTR		RN	11
SCR_NUM		RN	2 ;Screen number -- 0 is the cover screen, 1 is the +input pulse screen, etc.
PULSE		RN	8 ;Positive pulse duration (in clock cycles)
TIMER_LOC	RN	9 ;Timer location
__main

Program_uninit
		MOV	R0, #0
		MOV	R1, #0
		MOV	R2, #0
		MOV	R3, #0
		MOV	R4, #0
		MOV	R5, #0
		MOV	R6, #0
		MOV	R7, #0
		MOV	R8, #0
		MOV	R9, #0
		MOV	R10, #0
		MOV	R11, #0
		MOV	R12, #0
		;LDR	R13, =0x40000400 ;Reset stack
		;;In the event of a soft reset (triggered by '#')
		;;all registers should be cleared and program should start from the beginning
		
		LDR		r4, =(VICVectAddr)
		MOV		r5, #0
		STR		r5, [r4]
		
		LDR		r4, =(VICIntEnClr)	; Select VIC int clear reg
		MOV		r5, #TIMER2_INTMASK		
		STR		r5, [r4] ;Disable TIMER2
		
		LDR 	R4, =(T2IR)
		MOV		R5, #0xFF
		STR		R5, [R4]
		
		LDR		R0, =(T2TCR)
		LDR 	R2, [R0]     
		LDR		R1, =0x0   ;0 in 0 bit = off, 1 in 0 bit = on
		ORR		R2, R2, R1
		STR		R2, [R0]
		
		LDR		R0, =(T2TCR)
		LDR 	R2, [R0]     
		LDR		R1, =2_10   ;0 in 0 bit = off, 1 in 0 bit = on
		STR		R1, [R0]
		;;REset timer interrupt on soft reset
		
		LDR		R0, =(T2MR0) ;Match register 0
		MOV		R8, #0x0 ;Zero out R8
		STR		R8, [R0] ;Push value in R8 into MR0
		
		LDR		R0, =(T2MR1) ;Match register 1
		MOV		R8, #0x0 ;Zero out R8
		STR		R8, [R0] ;Push value in R8 into MR0
		
		
		BL timer2_init ;Set up timer
		
		; Since LEDs on the board will be used, configure pins of port 2 that are connected with LEDs
		; Enable LEDs (using PINSEL10)
		
		LDR		R4, =(SCS)			;look in .inc file
		LDR		R5, [R4]
		ORR		R5, R5, #0x01		;enable ports 0 and 1 as GPIO
		STR		R5, [R4]
		
		; Configure Port 4 as GPIO
		LDR		R4, =(PINSEL8)		;Set P4.15 .. P4.0 as GPIO
		MOV		R5, #0
		STR		R5, [R4]
		
		LDR		R4, =(FIO4DIR0)		;Select Port4 Direction register
		MOV		R5, #0xF0			;Set P4.7 .. P4.4 as output, P4.3 .. P4.0 as input
		STRB	R5, [R4]
		
		LDR		R4, =(PINMODE8)
		MOV		R5, #0x00AA			;0x00AA = 0b 0000 0000 1010 1010
		STR		R5, [R4]
		
		;LDR		R4, =(FIO4MASK0)
		;MOV		R5, #0x0F			;Apply mask to P4.3 .. P4.0
		;STR		R5, [R4]
		
		LDR		R4, =(FIO4PIN0)
		MOV		R5, #0x00			;Output low to P4.7 .. P4.4
		STR		R5, [R4]
		
init_LCD
		BL LCD_init
		
init_LED
		LDR		r4, =(FIO0DIR0)		; Select P0.0 to P0.7
		MOV		r5, #0xFF			; Set to Output
		STR		r5, [r4]			; Store changes
		
		LDR		R4, =(PINSEL0)
		MOV		R5, #0				; set port P0.7 .. P0.0 to GPIO
		STR		R5, [R4]	
		
		LDR		R4, =(PINMODE0)
		MOV		R5, #0x0     ; set port P0.6 to pinmode pull-up		
		STR		R5, [R4]
		
		BL LED_on
		
		MOV		R12, #4177920
		BL		BusyWait
		
		BL LED_off
		
		MOV		R12, #4177920
		BL		BusyWait
		
		BL LED_on
		
		MOV		R12, #4177920
		BL		BusyWait
		
		BL LED_off
		
		MOV		R12, #4177920
		BL		BusyWait
		
		
		
init_EINT0
		; Set up external interrupt functionality
		MOV		SIG_EN, #EN
		LDR		r4, =(PINSEL4)		; Select Port 2
		LDR		r5, [r4]			; Obtain register content
		ORR		r6, r5, r7, LSL #PIN_EINT0_OFS	; Change pin P2.10 function to EINT0
		STR		r6, [r4]			; Apply changes
		
		;Set EXTMODE
		LDR		r4, =(EXTMODE)
		MOV		r5, #0x01			; EXTMODE0 is ~~level-sensitive~~ edge
		STR		r5, [r4]
		
		;Set EXTPOLAR mode
		LDR		r4, =(EXTPOLAR)
		MOV		r5, #0x00			; EXTPOLAR0 is low-active
		STR		r5, [r4]
		
init_VIC_EINT0
		;Set address to VIC reg
		LDR		r4, =(VICVectAddr14)	; Select VIC vector address 0
		LDR		r5, =(EINT0_Handler)
		STR		r5, [r4]
		
		;Set interrupt priority
		LDR		r4, =(VICVectCntl4)
		MOV		r5, #1				; priority set to 1
		STR		r5, [r4]
		
		;Enable interrupt EINT0
		LDR		r4, =(VICIntEnable)	; Select VIC int enable reg
		LDR		r5, [r4]
		MOV		SIG_EN, #EINT0_INTMASK
		ORR		r5, r5, SIG_EN			; Enable EINT0
		STR		r5, [r4]
		
init_VIC_TIMER2
		LDR		r4, =(VICVectAddr26)	; Select VIC vector address 
		LDR		r5, =(TIMER2_Handler)
		STR		r5, [r4]
		
		LDR		R4, =(T2IR)
		MOV		R5, #0xFF
		STR		R5, [R4]
		
		;Set interrupt priority
		LDR		r4, =(VICVectCntl16)
		MOV		r5, #1				; priority set to 0
		STR		r5, [r4]
		
		;Enable interrupt TIMER0 INT
		LDR		r4, =(VICIntEnable)	; Select VIC int enable reg
		LDR		r5, [r4]
		MOV		SIG_EN, #TIMER2_INTMASK
		ORR		r5, r5, SIG_EN			; Enable TIMER2
		STR		r5, [r4]


		MOV		r3, #0		;Reset interrupt signal
		MOV		r10, #0		;Set counter to 0
		MOV		R9, #0x1	;Set LED state flag to on

;===================Cover_printing========================
		MOV		R0, #0x01 ;cls
		BL		LCD_command
		
		LDR		STR_PTR, =next_msg
		BL		LCD_string
		LDR		STR_PTR, =reset_msg
		BL		LCD_string
;=========================================================
	
		MOV	R2, #0
	
MainLoop
		TEQ		r3, #0		;Test if interrupt was entered -- Flag is IRQ=TRUE when R3 != 0
	
	
		
		BEQ		MainLoop
;============================Post-interrupt_sequence=========================
		CMP		R0, #'#'		;check if reset was entered (#)
		BEQ		__main			;Branch to main... Essentially a reset
		
		CMP		R3, #2 ;Timer IRQ
		BNE		Timer_IRQ_End
		
		;;;;
	;	LDR		r4, =(EXTINT)	;Clear External Interrupt flag
	;	LDR		r5, [r4]
	;	LDR		r6, =0x1				
	;	ORR		r5, r5, r6
	;	STR		r5, [r4]
	
		LDR 	R4, =(T2IR)
		MOV		R5, #0xFF
		STR		R5, [R4]
		B keypress_cleanup
Timer_IRQ_End	
		

		CMP		R0, #'*'		;check if next button pressed (*)
		BNE		keypress_capture		;GOTO keypress_capture
;next (*) switch-case		
		CMP		SCR_NUM, #0
		BEQ		screen0
		CMP		SCR_NUM, #1
		BEQ		screen1
		CMP		SCR_NUM, #2
		BEQ		screen2
		;What screen are we on?
		B keypress_capture ;Should never hit this Branch unless an error occured
		
screen0
		ADD		SCR_NUM, #1 ;Inc screen number
		;Don't do anything
		MOV		R0, #0x01 ;cls code
		BL		LCD_command ;clear
		
		LDR		STR_PTR, =ppulse_msg ;load text
		BL		LCD_string ;print text
		B 		keypress_capture ;exit case
		
screen1
		ADD		SCR_NUM, #1 ;Inc screen number
		BL		SCR1_code
		MOV		R0, #0x01 ;cls code
		BL		LCD_command ;clear
		
		LDR		STR_PTR, =npulse_msg ;load text
		BL		LCD_string ;print text
		B 		keypress_capture ;exit case
		
screen2 ;Not a real screen
		ADD		SCR_NUM, #1 ;Inc screen number
		BL		SCR2_code
		MOV		R0, #0x01 ;cls code
		BL		LCD_command ;clear
		B 		keypress_capture ;exit case
		


keypress_capture	
;R2 -- Screen number
;R8 -- pulse width
;R9 -- timer interrupt flag -- 0 determines off to on, 1 = on to off
		CMP		SCR_NUM, #1
			MOVEQ	R12, #10
			MULEQ	PULSE, R12, PULSE  ;Pulse = Pulse * 10
			SUBEQ	R0, R0, #'0' ;Convert ascii char into int
			ADDEQ	PULSE, PULSE, R0 ;Once the running total of PULSE is complete, push this value into the timer match register
			BEQ		keypress_cleanup
			
		CMP		SCR_NUM, #2
			MOVEQ	R12, #10
			MULEQ	PULSE, R12, PULSE  ;Pulse = Pulse * 10
			SUBEQ	R0, R0, #'0' ;Convert ascii char into int
			ADDEQ	PULSE, PULSE, R0 ;Once the running total of PULSE is complete, push this value into the timer match register
			BEQ		keypress_cleanup

keypress_cleanup
		MOV		R12, #255
		BL		BusyWait ;Give some delay so that a keypress doesn't print duplicate letters
		
		MOV		r3, #0		;Reset interrupt flag
		;Re-Enable interrupt
		LDR		r4, =(VICIntEnable)	; Select VIC int enable reg
		LDR		r5, [r4]
		MOV		SIG_EN, #EINT0_INTMASK
		ORR		r5, r5, SIG_EN			; RE-Enable EINT0
		STR		r5, [r4]
		
		LDR		r4, =(VICIntEnable)	; Select VIC int enable reg
		LDR		r5, [r4]
		MOV		SIG_EN, #TIMER2_INTMASK
		ORR		r5, r5, SIG_EN			; RE-Enable TIMER2
		STR		r5, [r4]
	
	

		
		

		B		MainLoop
;===========================Done_Post-interrupt_sequence==============================

;data
next_msg		DCB		"* for next", 0x00
reset_msg		DCB		"# to reset", 0x00
ppulse_msg		DCB		"Enter + pulse:", 0x00
npulse_msg		DCB		"Enter - pulse:", 0x00

		ALIGN
	
		END
