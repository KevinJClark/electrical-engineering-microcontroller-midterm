	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE	LPC23xx.inc ; MMR definitions
	EXPORT SCR2_code
	IMPORT	LED_on
	IMPORT	LED_off
	;Takes input from R8, Pulse width

SCR2_code
		STMIA	SP!, {R0-R2, LR}
		
		; Set the value of the Match Register (page 558)
		LDR		R0, =(T2MR0)
		LDR		R1, [R0]
		ADD		R8, R1, R8  ;Add initial numbers from first pulse
		LDR		R0, =(T2MR1) ;Match register 1
		STR		R8, [R0] ;Push value in R8 into MR1
		
		;Enable timer clock -- Start counting up
		LDR		R0, =(T2TCR)
		LDR 	R2, [R0]     
		LDR		R1, =0x1   ;0 in 0 bit = off, 1 in 0 bit = on
		;ORR		R2, R2, R1
		STR		R1, [R0]
	
		MOV		R8, #0x0 ;Zero out R8
		BL LED_on
		
		LDMDB	SP!, {R0-R2, PC}
		END