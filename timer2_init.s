; setup code for timer0
	
	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE LPC23xx.inc ; MMR definitions
	EXPORT	timer2_init



timer2_init
		; R0, R1, and R2 are all used in this subroutine to initialize the timer
		; Since these registers don't need to return any values to main, they should
		;   be placed on the stack
		STMIA	SP!, {R14, R0, R1, R2}	
		
		
		; refer to chapter 23 of user manual (page 554) for timer information
		
		; First step is to set bit 1 of the PCONP (power control for perihperals) register (page 69)
		LDR		R0, =(PCONP)	    ; puts address into R0
		LDR		R2, [R0]			; gets the value inside the PCONP address and stores it in R2
		LDR		R1, =0x400000       ; R1 now has a value of 1 at bit 22
		ORR		R2, R2, R1
		STR		R2, [R0]
		

		
		
		; fourth step is to setup the match control register
		; when the Timer Counter matches the Match Register, we need to 
		;   -- reset the Timer Counter        (bit 1 on page 559)
		LDR		R0, =(T2MCR) ;Match control register
		LDR 	R2, [R0]
		;Want an interrupt generated for MR0 and MR1 (bits 0 and 3) and additionally a timer value reset ONLY for MR1 (bit 4)
		LDR		R1, =2_11001
		ORR		R2, R2, R1
		STR		R2, [R0]
		
		; Modify the Timer Control Register (page 556-557)
		LDR		R0, =(T2TCR)
		LDR 	R2, [R0]     
		LDR		R1, =0x0   ;0 in 0 bit = off, 1 in 0 bit = on
		ORR		R2, R2, R1
		STR		R2, [R0]
		
		
		; modify the Count Control register (page 557 -- 558)
		; NOTE: We want the Timer Counter to increment on each rising clock edge and this is already default setting
		
		
		; put zero into the Prescale Counter register  (page 558)
		; This will increment the timer counter each time the Prescale regiseter = 0
		LDR		R0, =(T2PC) ;Prescale counter
		LDR 	R2, [R0]     
		LDR		R2, =0x0   
		STR		R2, [R0]
		

		
exit
		
		LDMDB	SP!, {PC, R0, R1, R2}
	
	END