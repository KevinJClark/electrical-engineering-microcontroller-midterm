	;1. Configure Pins used for LCD interfacing
	;2. Initialize LCD
	;	- 4 bit interface
	;	- turn on LCD
	;	- clear screen
	;	- set cursor to position 0
	;	- enable cursor blinking
	
	;/* PINS */
	;//Fill in pin number below
	;	RS	= P1.??
	;	RW	= P1.??
	;	E	= P1.??
	;	DB4	= P1.??
	;	DB5 = P1.??
	;	DB6 = P1.??
	;	DB7 = P1.??

	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE LPC23xx.inc	;MMR definitions
	EXPORT	LCD_init
	IMPORT	LCD_command
	IMPORT	BusyWait
	
CLEAR       EQU		0X01
HOME        EQU		0X02
LCD_ON      EQU		0X0C
BLINK       EQU		0X0F
CURSOR_ON   EQU		0X0E
LEFT        EQU		0X10
RIGHT       EQU		0X14
NEXT_LINE   EQU		0xC0
FOURBIT     EQU		0x28    ;0b00101000 = 0x28
LCD_OFF     EQU		0x0A
	
LCD_init

	STMIA	SP!, {R4-R7, R14}	;Preserve R4 thru R7 register
	
	LDR		R4, =(PINSEL3)
	MOV		R5, #0		   			;set port P1[31:16] as GPIO
	STR		R5, [R4]

	LDR		R4, =(FIO1DIR)	    	; each pin of the port is controlled by 1 bit
	LDR		R5, =0xFF000000			; to set P1.31 ... P0.24 as output (=1)
	LDR		R6, [R4]
	ORR		R6, R6, R5
	STR		R6, [R4]
	
	LDR		R12, =1000000		;Wait for LCD to "warm up"
	BL		BusyWait
	
	MOV		R0, #0x33			;Boot up
	BL		LCD_command
	
	MOV		R0, #0x32			;Boot up
	BL		LCD_command
	
    MOV		R0, #FOURBIT		;28H initialization to 2-lines
    BL 		LCD_command			;using 4-bit interface(DL = 0)

	MOV		R0, #BLINK			;turn LCD display on, cursor on	and blink
    BL 		LCD_command
	
	MOV		R0, #CLEAR		 	;clear LCD
    BL 		LCD_command
	
	MOV 	R0, #HOME			;or using 02H - return home command
    BL 		LCD_command
	
	LDMDB	SP!, {R4-R7, PC}	;Restore R4 thru R7 register

	END