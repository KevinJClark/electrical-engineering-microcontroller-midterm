	AREA Program, CODE, READONLY
	ARM 			;use ARM instruction set
	INCLUDE	LPC23xx.inc ; MMR definitions
	EXPORT	LCD_string
	IMPORT	LCD_data
	IMPORT	LCD_command

;R11 is input for string pointer.  Prints String located at <str_ptr> until it encounters a null (0x00) byte
		
STR_PTR		RN	11

LCD_string
		STMIA	SP!, {LR}
			
print
		LDRSB	R0, [STR_PTR], #1
		CMP		R0, #0x00 ;Null byte
		BEQ		done
		BL		LCD_data	 ;Print string to screen
		B		print
done
		MOV		R0, #0xC0 ;\n
		BL		LCD_command
		
		MOV		R0, #'0' ;;Prevents set to 0 so previous R0 char won't effect running total
		LDMDB	SP!, {PC}
		END